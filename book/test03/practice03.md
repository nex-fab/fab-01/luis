# 3D打印 练习记录
将[上次练习](https://nex-fab.gitlab.io/fab-01/luis/book/test02/practice02.html)中的座椅模型，通过3D打印机打印出来。  
## 安装Cura
[Cura下载直链](https://ultimaker.com/software/ultimaker-cura)   
选择下载，我用的电脑是Window 64位，所以点击的是Windows进行下载。  
下载后，直接安装，按照默认设置安装。  
![](https://gitlab.com/picbed/bed/uploads/dec2e07dc5ed00e76e846d1cbc187446/Cura下载.png)  
![](https://gitlab.com/picbed/bed/uploads/019d0e592bab3600524e93be3a3d1cb2/Cura下载02.png)  

## 导出stl文件 
使用fusion360打开画的模型  
选择生成——3D打印  
![](https://gitlab.com/picbed/bed/uploads/9d1d37d13baeb1b3c06046629957fe0b/生成文件.png)  
选中靠背实体，点击确定，并重命名保存  
![](https://gitlab.com/picbed/bed/uploads/099e075373bf4e56cba08cad10e8e0da/生成文件-靠背.png)  
![](https://gitlab.com/picbed/bed/uploads/77b380ddbd20f2ec5176b5add869bf00/生成文件-命名.png)  
同样，再导出扶手的stl文件  
![](https://gitlab.com/picbed/bed/uploads/0343b619d838a73f1e137cbddf039dfe/生成文件-扶手.png)  
连杆的圆直径为3mm，后续会将模型缩放到图纸的1/4以节省材料，缩短打印时间。所以最终连杆的圆直径将是0.75mm，而3D打印机支持的最小圆直径为0.8mm，因此连杆弃用，后续考虑用针或胶水代替。  

## 使用Cura生成gcode文件  
本次实操一共打印了两款，由于第一次的参数设置不对，因此打印出来模型很粗糙，是个失败品。而第二次调整参数后，相对较为精细。
### 失败模型操作过程
打开Cura  
![](https://gitlab.com/picbed/bed/uploads/f98c765702381f43c0e0bc2d4f016aab/Cura.png)  
导入配件  
![](https://gitlab.com/picbed/bed/uploads/9a9582e8d43f9d533e2c2499dd9db10a/导入配件.png)  
导入效果  
![](https://gitlab.com/picbed/bed/uploads/eafb8fb15e756e85d243b1081821cb7d/已导入.png)  
因为扶手需要两个，所以再次导入一个扶手  
![](https://gitlab.com/picbed/bed/uploads/d05788ce40aa4c9e7e0375c45d5db62e/再次导入扶手.png)  
旋转扶手  
![](https://gitlab.com/picbed/bed/uploads/bc1508c1287fd59ea8eee91550f29a1e/旋转扶手01.png)  
鼠标移动到绿线上，左键按住不放，开始拖动，看到度数变为90°就松手。此时扶手的侧面已经平行于底面  
![](https://gitlab.com/picbed/bed/uploads/cd47b299e3be32eeb52f25d94943bde5/旋转扶手02.png)  
缩放扶手为原尺寸的1/4，将红框中100%修改为25%，然后回车  
![](https://gitlab.com/picbed/bed/uploads/f654faafa624b14334b99e7e311617c9/缩放扶手.png)  
看到尺寸已经缩放为25%  
![](https://gitlab.com/picbed/bed/uploads/6dcbe88318fde31ce3b08454b075bf81/缩放扶手02.png)  
调整扶手Z轴坐标为0，使其贴近底面  
![](https://gitlab.com/picbed/bed/uploads/4c32dcd3e6ebeaabab24b94fde6b430b/调整坐标贴近底面.png)  
同样，将靠背缩小并贴近底面  
然后移动配件至合适位置。操作方法：点击移动按钮，选中所需移动的配件，然后左键按住不放，拖动即可移动  
![](https://gitlab.com/picbed/bed/uploads/7450c0e440833f038be0619780990659/移动配件.png)  
调整参数设置（此为失败品，不建议参考）  
![](https://gitlab.com/picbed/bed/uploads/1fae912a0ccaf0f9e05d1733855bbcad/打印参数设置01.png)  
![](https://gitlab.com/picbed/bed/uploads/d612a6412d0aa65f62f90a4f4777a459/打印参数设置02.png)  
![](https://gitlab.com/picbed/bed/uploads/e40fb3d2f58ace0ed01019b6a67b6d3f/打印参数设置03.png)  
点击预览，查看模拟的成品模样  
![](https://gitlab.com/picbed/bed/uploads/4d50e3e6463c62f3cddf0d8be8b8c162/预览.png)  
改变层高，可以查看对应层高的模拟模型  
![](https://gitlab.com/picbed/bed/uploads/102fcee6497d5bd91e6a9417b22ad3df/层高01.png)  
![](https://gitlab.com/picbed/bed/uploads/dc260962eb52381673137627fc5c1c0b/层高02.png)  
![](https://gitlab.com/picbed/bed/uploads/81c6862e72de25b6ed3be28694bed8fc/层高03.png)  
点击播放按钮，查看仿真动画  
![](https://gitlab.com/picbed/bed/uploads/eb1ce04940dc83b6a28a2d003e179164/模拟仿真.png)  
![](https://gitlab.com/picbed/bed/uploads/57479e0086e089f810a7218c6748acfd/仿真02.png)  
导出gcode文件，点击右下角slice按钮，然后再点击save to file按钮,命名并保存  
![](https://gitlab.com/picbed/bed/uploads/2da9e79b82bde532c5ceaf65af78fa8c/slice.png)![](https://gitlab.com/picbed/bed/uploads/1a3b414c54260655bde18779a0bf43f8/保存slice.png)  
![](https://gitlab.com/picbed/bed/uploads/953f74517a4cb246ecfd3f866133583a/生成打印文件.png)  

## 3D打印模型过程
将保存的gcode文件复制到SD卡的根目录中。SD卡如图所示：  
![](https://gitlab.com/picbed/bed/uploads/ba54111c4900422cc75919849a34f32c/SD卡.png)  
将SD卡插入3D打印机的卡槽  
![](https://gitlab.com/picbed/bed/uploads/248556f87dd1046a0a1df3e069a211df/SD卡插入打印机.png)  
下面检查材料，发现材料断裂，故卸下风扇，将材料重新安装  
拧下风扇的两颗螺丝，如图所示：  
![](https://gitlab.com/picbed/bed/uploads/90e401baa2f76324f55f24d0434e526c/拆卸风扇螺丝.png)  
将材料重新插入至喷头的孔中  
![](https://gitlab.com/picbed/bed/uploads/610a59d409fa5b6ef99456e327ce2f95/2.gif)  
看到侧面材料已入喷头孔  
![](https://gitlab.com/picbed/bed/uploads/d67b3a89154e9ba0dadbba38f29ec950/材料插入孔中.png)  
打开3D打印机背面的开关，红灯亮说明已启动  
![](https://gitlab.com/picbed/bed/uploads/eca8ba639384cb2583bef1213131f107/3D打印机开关.png)  
![](https://gitlab.com/picbed/bed/uploads/56faf7a3e8b3e5e8aeb92a49e6110da5/启动3D打印机.png)  
操作旋钮有两个功能，旋转按钮可以使光标移动进行选择，按压按钮可以确认选择  
![](https://gitlab.com/picbed/bed/uploads/b4fe299fc66d5af946a097ebce9f8559/操作按钮.png)  
选择Print from SD，按压按钮进行确认  
![](https://gitlab.com/picbed/bed/uploads/ec06ad986a327781b989236b6060faaf/选择打印.png)  
自动跳转至子目录  
![](https://gitlab.com/picbed/bed/uploads/f1b49c53795ec6a25b3104b03a293094/跳转页面.png)  
移动到打印文件，按压按钮进行确认  
![](https://gitlab.com/picbed/bed/uploads/8cedab94f0fcae7748df019e6c80adc1/选中打印文件.png)  
喷头开始加热，等待温度变为200℃  
![](https://gitlab.com/picbed/bed/uploads/ebe086cf654640641fe2bf964638b527/等待预热01.png)  
![](https://gitlab.com/picbed/bed/uploads/1758418fade27f0e595ff121d6bad7c6/等待预热05.png)  
到达200℃后，喷头复位至右上角，然后开始自动打印  
![](https://gitlab.com/picbed/bed/uploads/f244c8715a93ba5bc7932bf905b66e75/开始位置右上角.png)  
打印底层raft  
![](https://gitlab.com/picbed/bed/uploads/5b1e3fd1ddfa80898187ea6ccb6121b7/打印底层raft03.png)  
![](https://gitlab.com/picbed/bed/uploads/42f00901cd1c8a046dd765fedb53c065/打印底层raft04.png)  
![](https://gitlab.com/picbed/bed/uploads/bc2386d6c84b7a530bb4c2db5ee981b6/打印底层raft06.png)  
![](https://gitlab.com/picbed/bed/uploads/5af34ff343c2374d805e7a6214d5d0d3/打印底层raft08.png)  
模型打印  
![](https://gitlab.com/picbed/bed/uploads/f4015a4633c78420cad6fc0f0dd8d071/打印模型01.png)  
![](https://gitlab.com/picbed/bed/uploads/fa5fac6c082d51a55295b317b1007b22/打印模型02.png)  
由于模型看起来比较粗糙，终止打印，这个就是失败的案例  
![](https://gitlab.com/picbed/bed/uploads/b823d7a2043f4fbf32af6754bd2d38aa/失败模型.png)  

## 成功案例
打开Cura，重新配置参数  
![](https://gitlab.com/picbed/bed/uploads/c3543b44a645ef2c90ab32626c3fe1e8/参数01.png)  
![](https://gitlab.com/picbed/bed/uploads/bc22a7cf6904ede958ffa18b3d9793dc/参数02.png)  
开始打印，发现模型底座的线条有粗有细
![](https://gitlab.com/picbed/bed/uploads/189047c1f0335366654b48943316677f/线条粗细.png)  
细的位置高度不够，转动下方的螺丝，顺时针转动螺丝，则支架上升，逆时针转动螺丝，则支架下降。  
打印底层raft，可以看到比之前的失败品精细多了  
![](https://gitlab.com/picbed/bed/uploads/ba0d6240fff5b864b70fa5528d6f73d0/底座01.png)  
![](https://gitlab.com/picbed/bed/uploads/b101e3cfafeab39ae3cf61012e3a6eb9/底座02.png)  
![](https://gitlab.com/picbed/bed/uploads/659c5f4e9f49f3b4dbcb5b4399972541/底座03.png)  
![](https://gitlab.com/picbed/bed/uploads/5334131bdbd2136e7ba758475d675fc7/底座04.png)  
此次打印，由于材料比较脆弱，打印到中途材料断裂，导致打印被迫中断  
更换打印材料再次打印  
![](https://gitlab.com/picbed/bed/uploads/d48e30d2c3ee2240ef335d60abae4882/新材料底座.png)  

~~案例对比图，左边是失败品，右边是成功品（成功品还没完成~~~~~）  
