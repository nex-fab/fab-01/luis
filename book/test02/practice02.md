# fusion 360 练习记录
准备画一把椅子。
## 安装Fusion 360
https://www.nexmaker.com/doc/2cad/Fusion360prepare.html  
参考安装教程，进行软件的下载以及安装和注册。

新建设计，然后点击保存，命名。  
![](https://gitlab.com/picbed/bed/uploads/8975b17cac1918ab91c818800172a390/新建设计.png)  
![](https://gitlab.com/picbed/bed/uploads/7c07c6f686430c0af96c6d50a8f815f6/保存.png)  
![](https://gitlab.com/picbed/bed/uploads/55021f2a6cb1e2f4b958c7458df5ff31/命名.png)  

### 画扶手的草图  
先画一个大致的轮廓。  
注意：由于尺寸是估计的，所以一开始画的尺寸不是精确的，后面会贴上最终的尺寸，这里仅供参考。  
![](https://gitlab.com/picbed/bed/uploads/2d68493f7da4fce0565b020dd0e32d15/扶手轮廓.png)  
然后对棱角进行倒圆角处理，并画出连杆的圆用以后续装配，最终扶手的尺寸如下。
![](https://gitlab.com/picbed/bed/uploads/fe6f7d793ec9430a23a695bd4a60d5ba/扶手尺寸.png)  

### 画靠背的草图  
先画靠背的大致轮廓（尺寸同样不精准，后面会放最终尺寸）。  
![](https://gitlab.com/picbed/bed/uploads/aa14ae0dc866bd199de41938d5457509/靠背轮廓.png)  
然后对靠背进行旋转，匹配到扶手的圆。  
![](https://gitlab.com/picbed/bed/uploads/4efd73b626b0cfd9718ef11da14dc193/靠背旋转01.png)  
![](https://gitlab.com/picbed/bed/uploads/95e2ffd3ea2632c887386e8f4479b554/靠背旋转02.png)  
旋转到位置差不多的地方，按回车键进行确认。
![](https://gitlab.com/picbed/bed/uploads/fef091b5f27b0b9fef0345d6365f2fca/靠背旋转03.png)  
确定后，看到扶手草图的圆和靠背的中心差不多对上了。  
![](https://gitlab.com/picbed/bed/uploads/03881d2efd5a1847900ea84fe8d23013/靠背轴定位.png)  
对靠背草图在同样的位置画上两个直径为mm的圆，整个草图倒圆角处理。  
![](https://gitlab.com/picbed/bed/uploads/b44132aca34955147838c8f4684dc474/靠背轴定位__2_.png)  
使用重合功能，将两个圆心，都一一对应。  
![](https://gitlab.com/picbed/bed/uploads/0c49aeaca79fc18b12394fc91ece0a58/靠背轴定位__3_.png)  
看到两个圆心已经重合了。  
![](https://gitlab.com/picbed/bed/uploads/0b7b96002d5c67e59d59e28e8c93d9a8/靠背轴定位__4_.png)  
靠背草图最终尺寸如下。  
![](https://gitlab.com/picbed/bed/uploads/b7396f07d3c93c9c110aff95f1ff652e/靠背尺寸.png)  

### 靠背制作
利用拉伸功能，将靠背的实体图制作出来，注意不要选择那两个圆，这样实体图就是圆的位置是空心的。  
![](https://gitlab.com/picbed/bed/uploads/54ad885c798455b3dc2568077e9ce12d/靠背拉伸01.png)  
宽度设置100mm  
![](https://gitlab.com/picbed/bed/uploads/e3e8bb0897270885d392c5a2ca6aa7ce/靠背拉伸02.png)  
选中后可以看到，圆的位置是空心的圆柱。  
![](https://gitlab.com/picbed/bed/uploads/7cd0087e48adf8816db0a4499f622d3d/靠背拉伸03.png)  

### 扶手制作
利用拉伸功能，将靠背的实体图制作出来，注意不要选择那两个圆，这样实体图就是圆的位置是空心的。  
![](https://gitlab.com/picbed/bed/uploads/7b660ba66c87f7f7c05bbbf62e2c4e56/扶手拉伸.png)  
因为草图只有一个，而扶手需要两个。所以先从实体穿件零部件。  
![](https://gitlab.com/picbed/bed/uploads/b666f3c806b66b8503c5e66e82a7325e/扶手复制01.png)  
然后选中这个新的扶手零件，先按Ctrl+C（复制），再按Ctrl+V(粘贴)，输入移动的距离，这样就有两个扶手了。  
![](https://gitlab.com/picbed/bed/uploads/536cb29f745e116ac4c1fd25b7338fb0/扶手复制02.png)  


### 连杆制作  
新建零部件用于绘制连杆实体。  
![](https://gitlab.com/picbed/bed/uploads/546ef0d8fa32ae6c746bb0e009e3922e/新建零部件.png)  
然后再草图中，选中两个圆，利用拉伸，制作成实体。扶手是10mm，靠背是100mm，所以一共是120mm。  
![](https://gitlab.com/picbed/bed/uploads/56dc763e5b62a3cade77bf75851f5d35/连杆.png)  

## 全部零件的装配  
看一下所有的零件实物。  
![](https://gitlab.com/picbed/bed/uploads/fa5266a2739bc0d6daf9aa5f954266df/全配件.png)  
点击装配按钮，开始进行装配  
![](https://gitlab.com/picbed/bed/uploads/07f164edb72c6a1746eb2b34d96253b4/装配联接.png)  
先把左边扶手跟连杆装配在一起。  
零部件1选择上面连杆一个圆的边。  
![](https://gitlab.com/picbed/bed/uploads/d3d0450f3413ef23ce941897304a97b4/联接01.png)  
零部件2选择扶手左侧面的上面一个圆的边。  
![](https://gitlab.com/picbed/bed/uploads/7c9882175465c7d7eab4a6bad07f2faf/联接02.png)  
看到扶手与连杆已经装配上了。  
![](https://gitlab.com/picbed/bed/uploads/17efc59d9c3db166a822db7c93f2ad07/联接示意图01.png)  
再把靠背跟连杆装配在一起。
零部件1选择靠背左侧面上面一个圆的边。
![](https://gitlab.com/picbed/bed/uploads/06e3cd6c8fbc95913d1ed478eaba5dd5/联接03.png)  
零部件2选择已经装配的扶手右侧面上面一个圆的边。  
![](https://gitlab.com/picbed/bed/uploads/5e2d7a7c79a954975de719a433a3903a/联接04.png)  
接着把剩下的扶手跟连杆装配在一起。  
零部件1扶手右侧面的上面一个圆的边。  
![](https://gitlab.com/picbed/bed/uploads/514e112bc94d6ff319b631c61d502e32/联接05.png)  
零部件2选择连杆右侧面上面一个圆的边。  
![](https://gitlab.com/picbed/bed/uploads/9e2f25303f3084fd63ba67bd88a6c740/联接06.png)  
最终成品  
![](https://gitlab.com/picbed/bed/uploads/841e0472570fa57b633635095be81ee6/成品.png)  
[成品链接](https://a360.co/3axzDdP)  
模型预览  
<iframe src="https://nexpcb4.autodesk360.com/shares/public/SH56a43QTfd62c1cd968fde3c515bfd00d0d?mode=embed" width="1024" height="768" allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true"  frameborder="0"></iframe>